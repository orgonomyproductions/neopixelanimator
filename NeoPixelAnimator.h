#ifndef NeoPixelAnimator_h
#define NeoPixelAnimator_h

#include "Color.h"
#include "neopixel.h"

#define UPDATE_TIME 30

class Animation{
  public:
      Animation(){
          currentStep = 0;
      }
      virtual Animation* clone() = 0;
      virtual void update(int nPixels, int updateTime, Adafruit_NeoPixel &strip) = 0;
      Color getCurrentColor(){
          return currentColor;
      }
      Color getBeginColor(){
          return beginColor;
      }
      void reset(){
          currentColor = beginColor;
          currentStep = 0;
      }

  protected:
      Color currentColor, beginColor;
      int currentStep;

      void setAll(int nPixels, Color color, Adafruit_NeoPixel &strip){
          for(int i=0;i<nPixels;i++){
            strip.setPixelColor(i, strip.Color(color.r,color.g,color.b));
          }
          strip.show();
      }
};


class BouncingPixelAnimation: public Animation{
  public:
    BouncingPixelAnimation();
    BouncingPixelAnimation(Color _beginColor, int _duration){
      duration = _duration;
      bounceDirection = true;
      beginColor = _beginColor;
      pixelPosition = 0;
    }
    BouncingPixelAnimation* clone(){
      return new BouncingPixelAnimation(*this);
    }

    void update(int nPixels, int updateTime, Adafruit_NeoPixel &strip){
      int nSteps = duration/(2*updateTime);

      float pixelPerUpdate;
      if (bounceDirection){
        pixelPerUpdate = (float)nPixels/nSteps;
      }else{
          pixelPerUpdate = -(float)nPixels/nSteps;
      }

      setAll(nPixels, beginColor, strip);
      strip.setPixelColor(int(pixelPosition), strip.Color(255,255,255));

      pixelPosition+=pixelPerUpdate;
      if (pixelPosition > nPixels || pixelPosition < 0){
          bounceDirection = !bounceDirection;
      }

      strip.show();
    }
  protected:
    int duration;
    bool bounceDirection;
    float pixelPosition;

};

class SparklingAnimation : public Animation{
  public:
    SparklingAnimation();
    SparklingAnimation(Color _beginColor, int _duration){
      duration = _duration;
      beginColor = _beginColor;
      lastPixel = -1;
    }
    SparklingAnimation* clone(){
      return new SparklingAnimation(*this);
    }
    void update(int nPixels, int updateTime, Adafruit_NeoPixel &strip){
      int pixel = random(nPixels-1);
      if (pixel == lastPixel) lastPixel+=1;
      lastPixel = pixel;
      setAll(nPixels, beginColor, strip);
      strip.setPixelColor(int(pixel), strip.Color(255,255,255));

      strip.show();

    }
  protected:
    int duration;
    int lastPixel;

};

class BreathAnimation: public Animation{
    public:
        BreathAnimation();
        BreathAnimation(Color _beginColor, Color _endColor, int _duration){
            duration = _duration;
            breathInc = true;

            beginColor = _beginColor;
            endColor = _endColor;
            currentColor = beginColor;

        };
        BreathAnimation* clone(){
          return new BreathAnimation(*this);
        }

        void update(int nPixels, int updateTime, Adafruit_NeoPixel &strip){
            int nSteps = duration/(2*updateTime);
            currentStep++;

            //Symmetric rounding
            int rInc, gInc, bInc;
            if((float)(endColor.r - beginColor.r)/nSteps > 0){
              rInc = ceil((float)(endColor.r - beginColor.r)/nSteps);
            }else{
              rInc = floor((float)(endColor.r - beginColor.r)/nSteps);
            }

            if((float)(endColor.g - beginColor.g)/nSteps > 0){
              gInc = ceil((float)(endColor.g - beginColor.g)/nSteps);
            }else{
              gInc = floor((float)(endColor.g - beginColor.g)/nSteps);
            }

            if((float)(endColor.b - beginColor.b)/nSteps > 0){
              bInc = ceil((float)(endColor.b - beginColor.b)/nSteps);
            }else{
              bInc = floor((float)(endColor.b - beginColor.b)/nSteps);
            }

            if (breathInc){
                currentColor.r = constrain(currentColor.r + rInc, 0, 255);
                currentColor.g = constrain(currentColor.g + gInc, 0, 255);
                currentColor.b = constrain(currentColor.b + bInc, 0, 255);
            } else{
                currentColor.r = constrain(currentColor.r - rInc, 0, 255);
                currentColor.g = constrain(currentColor.g - gInc, 0, 255);
                currentColor.b = constrain(currentColor.b - bInc, 0, 255);
            }

            if (currentStep == nSteps+1){
                breathInc = !breathInc;
                currentStep = 0;
            }

            setAll(nPixels, currentColor, strip);
        }

        Color getEndColor(){
            return endColor;
        }

    protected:
        Color endColor;
        int duration;
        bool breathInc;
};



class Transition : public Animation{
    public:
        Transition* clone() = 0;
        void setBeginColor(Color _beginColor){
            beginColor = _beginColor;
            currentColor = beginColor;
        }
        void setEndColor(Color _endColor){
            endColor = _endColor;
        }
        bool isFinished(){
            return bFinished;
        }
    protected:
        Color beginColor, endColor, currentColor;
        int duration;
        bool bFinished;
};

class FadeTransition: public Transition{
    public:
        FadeTransition(int _duration){
            duration = _duration;
        }
        FadeTransition* clone(){
          return new FadeTransition(*this);
        }
        void update(int nPixels, int updateTime, Adafruit_NeoPixel &strip){
            bFinished = false;
            int nSteps = duration/updateTime;
            currentStep++;

            //Symmetric rounding
            int rInc, gInc, bInc;
            if((float)(endColor.r - beginColor.r)/nSteps > 0){
              rInc = ceil((float)(endColor.r - beginColor.r)/nSteps);
            }else{
              rInc = floor((float)(endColor.r - beginColor.r)/nSteps);
            }

            if((float)(endColor.g - beginColor.g)/nSteps > 0){
              gInc = ceil((float)(endColor.g - beginColor.g)/nSteps);
            }else{
              gInc = floor((float)(endColor.g - beginColor.g)/nSteps);
            }

            if((float)(endColor.b - beginColor.b)/nSteps > 0){
              bInc = ceil((float)(endColor.b - beginColor.b)/nSteps);
            }else{
              bInc = floor((float)(endColor.b - beginColor.b)/nSteps);
            }

            currentColor.r = constrain(currentColor.r + rInc, 0, 255);
            currentColor.g = constrain(currentColor.g + gInc, 0, 255);
            currentColor.b = constrain(currentColor.b + bInc, 0, 255);

            if (currentStep == nSteps + 1){
                bFinished = true;
                currentStep = 0;

            }
            setAll(nPixels, currentColor, strip);
        }
};

class WipeTransition:public Transition{
    public:
        WipeTransition(int _duration){
            duration = _duration;
            wipeOut = true;
            bWiping = false;
            wipedPixel = 0;
        }
        WipeTransition* clone(){
          return new WipeTransition(*this);
        }

        void update(int nPixels, int updateTime, Adafruit_NeoPixel &strip){
            int nSteps = duration/(2*updateTime);
            bWiping = true;
            float pixelPerUpdate = (float)nPixels/nSteps;
            if (wipeOut){
                wipedPixel+=pixelPerUpdate;
                for (int i=0; i<(int)wipedPixel;i++){
                    strip.setPixelColor(i, strip.Color(0,0,0));
                }


                if (wipedPixel > nPixels){
                    wipeOut = false;
                    wipedPixel = 0;
                }
            }else{
                wipedPixel+=pixelPerUpdate;
                for (int i=0; i<wipedPixel;i++){
                    strip.setPixelColor(i, strip.Color(endColor.r,endColor.g,endColor.b));
                }
                if (wipedPixel > nPixels){
                    wipeOut = true;
                    wipedPixel = 0;
                    bWiping = false;
                }
            }
            bFinished = !bWiping;
            strip.show();
        }
    protected:
        bool wipeOut;
        float wipedPixel;
        bool bWiping;
};

class OverlapWipeTransition:public WipeTransition{
    public:
        OverlapWipeTransition(int _duration):WipeTransition(_duration){
        };
        OverlapWipeTransition* clone(){
          return new OverlapWipeTransition(*this);
        }

        void update(int nPixels, int updateTime, Adafruit_NeoPixel &strip){
            int nSteps = duration/updateTime;
            bWiping = true;
            float pixelPerUpdate = (float)nPixels/nSteps;

            wipedPixel+=pixelPerUpdate;
            for (int i=0; i<(int)wipedPixel;i++){
                strip.setPixelColor(i, strip.Color(endColor.r,endColor.g,endColor.b));
            }
            strip.show();

            if (wipedPixel > nPixels){
                wipedPixel = 0;
                bWiping = false;
            }
            bFinished = !bWiping;
        }
};

class FadeWipeTransition:public WipeTransition{
    public:
        FadeWipeTransition(int _duration):WipeTransition(_duration){
        };
        FadeWipeTransition* clone(){
          return new FadeWipeTransition(*this);
        }

        void update(int nPixels, int updateTime, Adafruit_NeoPixel &strip){
            int nSteps = duration/updateTime;
            currentStep++;

            //Symmetric rounding
            int rInc, gInc, bInc;
            if((float)(endColor.r - beginColor.r)/nSteps > 0){
              rInc = ceil((float)(endColor.r - beginColor.r)/(nSteps*0.5));
            }else{
              rInc = floor((float)(endColor.r - beginColor.r)/(nSteps*0.5));
            }

            if((float)(endColor.g - beginColor.g)/nSteps > 0){
              gInc = ceil((float)(endColor.g - beginColor.g)/(nSteps*0.5));
            }else{
              gInc = floor((float)(endColor.g - beginColor.g)/(nSteps*0.5));
            }

            if((float)(endColor.b - beginColor.b)/nSteps > 0){
              bInc = ceil((float)(endColor.b - beginColor.b)/(nSteps*0.5));
            }else{
              bInc = floor((float)(endColor.b - beginColor.b)/(nSteps*0.5));
            }


            bWiping = true;
            float pixelPerUpdate = (float)nPixels/(nSteps*0.5);

            wipedPixel+=pixelPerUpdate;
            for (int i=0; i<min((int)wipedPixel,nPixels);i++){
                int r,g,b;

                if (rInc > 0) r = min(uint8_t(strip.getPixelColor(i) >> 16) + rInc, endColor.r);
                else r = max(uint8_t(strip.getPixelColor(i) >> 16)  + rInc, endColor.r);
                if (gInc > 0) g = min(uint8_t(strip.getPixelColor(i) >> 8)  + gInc, endColor.g);
                else g = max(uint8_t(strip.getPixelColor(i) >> 8) + gInc, endColor.g);
                if (bInc > 0) b = min(uint8_t(strip.getPixelColor(i)) + bInc, endColor.b);
                else b = max(uint8_t(strip.getPixelColor(i)) + bInc, endColor.b);

                strip.setPixelColor(i, strip.Color(r,g,b));
            }
            strip.show();

            if (currentStep == nSteps + 1){
                wipedPixel = 0;
                bWiping = false;
                currentStep = 0;
            }

            bFinished = !bWiping;
        }
};


class NeoPixelAnimator {

public:
    ~NeoPixelAnimator(){
      delete strip;
      delete currentAnimation;
      delete currentTransition;
    }
    void setup(int _nPixels, int pin, int _updateTime = UPDATE_TIME){
        nPixels = _nPixels;
        // strip = Adafruit_NeoPixel(nPixels, pin, WS2812B);
        // Adafruit_NeoPixel strip;
        strip = new Adafruit_NeoPixel(nPixels, pin, WS2812B);
        strip->begin();

        updateTime = _updateTime;
        bTransition = false;

    }
    void setAnimation(Animation &animation){
        currentAnimation = animation.clone();
        currentAnimation->reset();
    }
    void setAnimation(Animation &animation, Transition &transition){
        currentTransition = transition.clone();
        currentTransition->setBeginColor(currentAnimation->getCurrentColor());
        currentTransition->setEndColor(animation.getBeginColor());
        currentAnimation = animation.clone();
        currentAnimation->reset();
        bTransition = true;
    }

    void update(){
      if (millis() - lastUpdate > updateTime){
          if (!bTransition){
                currentAnimation->update(nPixels, UPDATE_TIME, *strip);
          } else {
              currentTransition->update(nPixels, UPDATE_TIME, *strip);
              if (currentTransition->isFinished()){
                    bTransition = false;
                }
          }
          lastUpdate = millis();
      }
    }

private:
    Adafruit_NeoPixel *strip;
    int nPixels;

    Animation* currentAnimation;
    Transition* currentTransition;

    unsigned long lastUpdate;
    int updateTime;
    int duration;

    bool bTransition;
};




#endif
