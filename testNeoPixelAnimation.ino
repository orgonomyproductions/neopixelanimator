#include <NeoPixelAnimator.h>

BreathAnimation breathAnimationA = BreathAnimation(Color(255,0,0), Color(100,0,0), 1000);
BreathAnimation breathAnimationB = BreathAnimation(Color(0,255,10), Color(20,0,255), 1000);
/*BouncingPixelAnimation bp = BouncingPixelAnimation*/

// Uncomment just the transition to use
//FadeTransition transition = FadeTransition(2000);
/*WipeTransition transition = WipeTransition(2000);*/
//OverlapWipeTransition transition = OverlapWipeTransition(2000);
FadeWipeTransition transition = FadeWipeTransition(2000);

NeoPixelAnimator animator;

unsigned long lastUpdate;
bool animationA = true;
void setup() {
  delay(500);
  animator.setup(6, D2);
  animator.setAnimation(breathAnimationA);
  lastUpdate = millis();
}


void loop() {
  if (millis() - lastUpdate > 6000){
    animationA = !animationA;
    if (animationA){
      animator.setAnimation(breathAnimationA, transition);
    }else{
      animator.setAnimation(breathAnimationB, transition);
    }
    lastUpdate = millis();
  }
  animator.update();
}
